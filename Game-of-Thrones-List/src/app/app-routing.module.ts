import { CharacterDetailModule } from './pages/character-detail/character-detail.module';
import { CharacterListModule } from './pages/character-list/character-list/character-list.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterDetailComponent } from './pages/character-detail/character-detail.component';
import { CharacterListComponent } from './pages/character-list/character-list.component';
import { HomeComponent } from './pages/home/home.component';

//LazyLoad = utilizamos loadChildren para cargar módulos en vez de cargar sólo componentes
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('src/app/pages/home/home.module').then(m => m.HomeModule)
  },
  //ruta de lista de characters
  {
    //lo que va a poner el usuario en el navegador
    path: 'character-list',
    //lo que tiene que cargar
    loadChildren: () => import('src/app/pages/character-list/character-list/character-list.module').then(m => m.CharacterListModule)
  },
  {
    path: 'detail/:characterId',
    loadChildren: () => import('src/app/pages/character-detail/character-detail.module').then(m => m.CharacterDetailModule)
  },
  { path: 'create-character',
    loadChildren: () => import('src/app/pages/create-character/create-character.module').then(m => m.CreateCharacterModule)
  },
  { path: 'edit-character/:characterId',
    loadChildren: () => import('src/app/pages/edit-character/edit-character.module').then(m => m.EditCharacterModule)

  },
  {
    path: '**',
    component: HomeComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
