import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public selectedRoute?: string;

  constructor(private router: Router) {}

  ngOnInit(): void {
    //navegaciones entre las distintas páginas
    this.router.events.subscribe((event) => {
      //usamos instanceof para saber de qué tipo es un objeto. Sólo se ejecuta
      //lógica cuando el evento es sólo de tipo NavigationEnd
      if (event instanceof NavigationEnd) {
        this.selectedRoute = event.url.split('/')[1];
      }
    });
  }
}
