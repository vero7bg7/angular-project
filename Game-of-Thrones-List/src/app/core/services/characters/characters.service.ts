import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICharacter } from './models/character.models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  //permite hacer las peticiones a la API
  constructor(private httpClient: HttpClient) {}

  //método para recuperar los personajes de la API
  //se utiliza Obserbable para tratar la asincronía
  //*ERROR: apiUrl no se comunica desde Environment
  public getCharacters(): Observable<ICharacter[]> {
    return this.httpClient.get<ICharacter[]>('https://62964109810c00c1cb7248cd.mockapi.io/characters');
  }

  //método para recuperar los personajes por ID
  public getCharacterById(characterId: string): Observable<ICharacter> {
    return this.httpClient.get<ICharacter>(`https://62964109810c00c1cb7248cd.mockapi.io/characters/${characterId}`);
  }

  //método para crear personaje
  public addCharacter(body: ICharacter): Observable<ICharacter>{
   return this.httpClient.post<ICharacter>('https://62964109810c00c1cb7248cd.mockapi.io/characters', body);
  }

  //método para modificar personaje
  public editCharacter(idCharacter: string, body: ICharacter): Observable<ICharacter>{
    return this.httpClient.put<ICharacter>(`https://62964109810c00c1cb7248cd.mockapi.io/characters/${idCharacter}`, body);
   }

  //método para eliminar personaje
  public deleteCharacter(idCharacter: string): Observable<ICharacter> {
    return this.httpClient.delete<ICharacter>(`https://62964109810c00c1cb7248cd.mockapi.io/characters/${idCharacter}`);
  }

};
