export interface ICharacter {
      id: string;
      firstName: string;
      lastName:  string;
      fullName:  string;
      title:  string;
      family:  string;
      image:  string;
      imageUrl:  string;
}

export interface IPostCharacterResponse {
      id: string;
}