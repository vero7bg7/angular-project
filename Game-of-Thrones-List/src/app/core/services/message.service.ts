import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public message?: string;


  constructor() { }

  public getMessage(): string{
    return this.message || '';
  }

  public setMessage(message: string) {
    this.message = message;
  }
}
