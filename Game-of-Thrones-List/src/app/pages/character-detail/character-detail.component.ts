import { CharactersService } from 'src/app/core/services/characters/characters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { characters } from '../character-list/character-list.config';
import { ICharacter } from 'src/app/core/services/characters/models/character.models';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss'],
})
export class CharacterDetailComponent implements OnInit {
  //public characters: ICharacter[] = characters as ICharacter[];
  public currentCharacter?: ICharacter;

  constructor(private activatedRoute: ActivatedRoute,
    private characterService: CharactersService) {}

  //Función para mostrar a los personajes por su ID, buscando en toda la lista de personajes
  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const characterId = params['characterId'];
      this.characterService.getCharacterById(characterId).subscribe((character) => {
        this.currentCharacter = character;
      });
    });
  }
}
