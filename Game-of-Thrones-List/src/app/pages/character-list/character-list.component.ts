import { ICharacter } from './../../core/services/characters/models/character.models';
import { Component, OnInit } from '@angular/core';
import { characters } from './character-list.config';
import { MessageService } from 'src/app/core/services/message.service';
import { CharactersService } from 'src/app/core/services/characters/characters.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss'],
})
export class CharacterListComponent implements OnInit {
  public characters?: ICharacter[];
  //propiedad para crear los filtrados
  public filteredCharacters?: ICharacter[] = this.characters;
  public canEdit: boolean = false;
  //propiedad que da valor a input ngModels y permite modificar en ambos sentidos
  public filterValue: string = '';
  public message: string = '';

  constructor(
    private messageService: MessageService,
    private charactersService: CharactersService
  ) {}

  //guardo en mi variable ICharacter el valor que recibo de la API
  ngOnInit(): void {
    this.getCharacters();
  }
  //creo la función onEdit añadida al button del html
  public onEdit() {
    this.canEdit = !this.canEdit;
  }

  //creo la función onDelete añadida en el padre para eliminar personajes por su id
  public onDelete(id: string) {
    //me filtrará el personaje cuya id sea distinto al id que quiera eliminar
    this.charactersService.deleteCharacter(id).subscribe((character) => {
      console.log('Delete', character);
      //llamo a la función getCharacters para traer a los personajes tras eliminar
      this.getCharacters();
    });
  }

  public onFilter() {
    //filtramos todos las FAMILIAS de personajes que incluya lo que escribe el usuario en el input
    this.filteredCharacters = this.characters?.filter((character) => {
      return character.family
        .toLowerCase()
        .includes(this.filterValue.toLowerCase());
    });
  }

  public sendMessage() {
    this.messageService.setMessage(this.message);
  }

  //creo función para llamar de nuevo a la API cada vez que elimine personajes
  private getCharacters() {
    this.charactersService.getCharacters().subscribe((characters) => {
      this.characters = characters;
      this.filteredCharacters = characters;
      //  this.charactersService.addCharacter({
      //  ...characters[0],
      //   fullName: "Veronica",
      //   family: "Targaryen"
      // }).subscribe((res) => console.log(res));
    });
  }
}
