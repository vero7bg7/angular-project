import { CharacterDetailComponent } from './../../character-detail/character-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CharacterListComponent } from './../character-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterComponent } from '../components/character/character.component';
import { RouterModule } from '@angular/router';
import { CharacterListRoutingModule } from '../character-list-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CharacterDetailModule } from '../../character-detail/character-detail.module';



@NgModule({
  declarations: [
    CharacterListComponent,
    CharacterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    CharacterListRoutingModule,
    SharedModule,
    CharacterDetailModule
  ]
})
export class CharacterListModule { }
