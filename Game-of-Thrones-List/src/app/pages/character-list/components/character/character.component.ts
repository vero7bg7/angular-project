import { ICharacter } from './../../../../core/services/characters/models/character.models';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  //uso input para comunicar con su padre y le digo que es del tipo ICharacter
  //Lo paso desde el character-list
  @Input() public character?: ICharacter;
  @Input() public canDelete?: boolean = false;
  @Output() public delete: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  //creo la función onDelete con su Output
  public onDelete() {
    this.delete.emit();
}

}
