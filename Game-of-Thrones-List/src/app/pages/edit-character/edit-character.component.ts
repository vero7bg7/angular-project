import { CharactersService } from 'src/app/core/services/characters/characters.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { ICharacter } from 'src/app/core/services/characters/models/character.models';

@Component({
  selector: 'app-edit-character',
  templateUrl: './edit-character.component.html',
  styleUrls: ['./edit-character.component.scss'],
})
export class EditCharacterComponent implements OnInit {

  public character?: ICharacter;

  constructor(
    private activatedRoute: ActivatedRoute,
    private charactersService: CharactersService
  ) {}

  //1-recupero character id de la ruta
  //2-obtengo el character con el id llamando a la api
  //
  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const characterId = params['characterId'];
      this.charactersService.getCharacterById(characterId).subscribe((character) => {
        this.character = character;
      })
    });
  }
}
