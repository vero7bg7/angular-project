import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCharacterRoutingModule } from './edit-character-routing.module';
import { EditCharacterComponent } from './edit-character.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    EditCharacterComponent
  ],
  imports: [
    CommonModule,
    EditCharacterRoutingModule,
    SharedModule
  ]
})
export class EditCharacterModule { }
