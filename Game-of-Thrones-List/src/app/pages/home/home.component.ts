import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //variable para mostrar el mensaje
  public message: string = '';

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.message = this.messageService.getMessage()
  }

}
