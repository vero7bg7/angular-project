import { Router } from '@angular/router';
import { CharactersService } from 'src/app/core/services/characters/characters.service';
import { Component, Input } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ICharacter } from 'src/app/core/services/characters/models/character.models';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent {
  @Input() public character?: ICharacter;
  @Input() public EditMode: boolean = false;

  public characterForm?: FormGroup;

  constructor(
    private fb: FormBuilder,
    private charactersService: CharactersService,
    private router: Router
  ) {
    this.characterForm = this.fb.group({
      firstName: new FormControl(
        this.character ? this.character.firstName : '',
        [Validators.required]
      ),
      lastName: new FormControl(this.character ? this.character.lastName : '', [
        Validators.required,
      ]),
      fullName: new FormControl(this.character ? this.character.fullName : '', [
        Validators.required,
      ]),
      title: new FormControl(this.character ? this.character.title : '', [
        Validators.required,
      ]),
      family: new FormControl(this.character ? this.character.family : '', [
        Validators.required,
      ]),
      imageUrl: new FormControl(this.character ? this.character.imageUrl : '', [
        Validators.required,
      ]),
    });
  }

  public saveCharacter() {
    const formValue = this.characterForm?.value;
    const characterAdd$ =
      this.EditMode && this.character
        ? this.charactersService.editCharacter(this.character.id, formValue)
        : this.charactersService.addCharacter(formValue);
    characterAdd$.subscribe((character) => {
      console.log(character);
      this.router.navigate(['/character-list']);
    });
  }
}
